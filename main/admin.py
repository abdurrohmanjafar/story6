from django.contrib import admin
from .models import Kegiatan, Person
# Register your models here.
admin.site.register(Kegiatan)
admin.site.register(Person)
