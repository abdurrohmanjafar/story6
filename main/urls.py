from django.urls import path
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('tambah_kegiatan/', views.tambah_kegiatan, name='tambah_kegiatan'),
    path('tambah_orang/<int:id>/', views.tambah_orang, name='tambah_orang'),
]
