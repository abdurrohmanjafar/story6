from django.test import TestCase, Client
from django.urls import resolve
from .views import tambah_kegiatan, tambah_orang, home
from .models import Kegiatan, Person



class TestHome(TestCase):
    def test_home_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_home_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_home_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'kegiatan.html')


class TestTambahKegiatan(TestCase):
    def test_tambah_Kegiatan_url_is_exist(self):
        response = Client().get('/tambah_kegiatan/')
        self.assertEqual(response.status_code, 200)

    def test_tambah_Kegiatan_index_func(self):
        found = resolve('/tambah_kegiatan/')
        self.assertEqual(found.func, tambah_kegiatan)

    def test_tambah_Kegiatan_POST_(self):
        response = Client().post('/tambah_kegiatan/', {"nama_kegiatan" : "abcd", "deskripsi_kegiatan" : "abcd"} )
        self.assertEqual(response.status_code, 302)

    def test_tambah_Kegiatan_using_template(self):
        response = Client().get('/tambah_kegiatan/')
        self.assertTemplateUsed(response, 'tambah_kegiatan.html')

    def test_Kegiatan_model_create_new_object(self):
        acara = Kegiatan(nama_kegiatan="abc", deskripsi_kegiatan = "abcd" )
        acara.save()
        self.assertEqual(Kegiatan.objects.all().count(), 1)


class TestTambahOrang(TestCase):
    def setUp(self):
        acara = Kegiatan(nama_kegiatan="abc", deskripsi_kegiatan = "abcd")
        acara.save()

    def test_add_Person_POST(self):
        response= Client().post('/tambah_orang/1/', {"person_name" : "abcd"})
        self.assertEqual(response.status_code, 302)

    def test_add_Person_index_func(self):
        found = resolve('/tambah_orang/1/')
        self.assertEqual(found.func, tambah_orang)

    def test_add_Person_url_is_exist(self):
        response = Client().post('/tambah_orang/1', data={'person_name': 'Jafar'})
        self.assertEqual(response.status_code, 301)

class TestModel(TestCase):
    def test_str_model_Kegiatan(self):
        kegiatan = Kegiatan.objects.create(nama_kegiatan ='pepewe', deskripsi_kegiatan = "asik")
        self.assertEqual(kegiatan.__str__(), 'pepewe')

    def test_str_model_member(self):
        kegiatan = Kegiatan.objects.create(nama_kegiatan='pepewe', deskripsi_kegiatan = "asik")
        orang = Person.objects.create(person_name="memberTest", kegiatan = kegiatan)
        self.assertEqual(orang.__str__(), 'memberTest')
