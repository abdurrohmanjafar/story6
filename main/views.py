from django.shortcuts import render,redirect
from .models import Kegiatan,Person, Form_Kegiatan, Form_Person

def home(request):
    persons = Person.objects.all()
    kegiatans = Kegiatan.objects.all()
    return render(request, 'kegiatan.html', {"person" : persons, "kegiatan" : kegiatans})

def tambah_kegiatan(request):
    if (request.method == "POST"):
        form = Form_Kegiatan(request.POST)
        if form.is_valid():
            form.save()
            return redirect("main:home")
    else:
        form = Form_Kegiatan()
        return render(request, "tambah_kegiatan.html",{"form" : form})

def tambah_orang(request, id):
    if (request.method == "POST"):
        form = Form_Person(request.POST)
        if form.is_valid():
            kegiatan = Kegiatan.objects.get(id=id)
            form = Person(kegiatan = kegiatan, person_name= form.data["person_name"])
            form.save()
            return redirect("main:home")
    else:
        form = Form_Person()
        return render(request, "tambah_orang.html",{"form" : form})

