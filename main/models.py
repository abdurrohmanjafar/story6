from django.db import models
from django.forms import ModelForm

class Kegiatan (models.Model):
    nama_kegiatan = models.CharField(max_length = 50)
    deskripsi_kegiatan = models.TextField("deskripsi")
    def __str__(self):
        return (self.nama_kegiatan)

class Person (models.Model):
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE, default=0)
    person_name = models.CharField("nama lengkap",max_length = 50)
    def __str__(self):
        return self.person_name
class Form_Kegiatan (ModelForm):
    class Meta:
        model = Kegiatan
        fields = "__all__"
class Form_Person (ModelForm):
    class Meta:
        model = Person
        fields = ["person_name"]
# Create your models here.
